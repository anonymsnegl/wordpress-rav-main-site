<?php
/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        StanleyWP 
 * @author         Brad Williams & Carlos Alvarez 
 * @copyright      2011 - 2014 Gents Themes
 * @license        license.txt
 * @version        Release: 3.0.3
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */
?>
</div><!-- end of wrapper-->
<?php gents_wrapper_end(); // after wrapper hook ?>


<?php gents_container_end(); // after container hook ?>


  <!-- +++++ Footer Section +++++ -->
<div class="container ">
      <div class="row">
  <footer id="footer" class="col-lg-8 col-lg-offset-2">
  <hr>

        <div class="col-lg-4">
          <!--?php dynamic_sidebar('footer-left'); ?-->
		  <span id="copy"><strong> RAV 2014</strong></span>    
		  <span id="kontakt-os">KONTAKT OS </span>
        </div>
        <div class="col-lg-4" style="text-align:center;">
          <!--?php dynamic_sidebar('footer-middle'); ?-->
		  <p>LUNDHOLMVEJ 55<br/>7500 HOLSTEBRO</p>
        </div>
        <div class="col-lg-4">
		<span id="tlf">75572216</span>
		<span id="mail">RAV@RAV-HOLSTEBRO.DK</span>
          <!--?php dynamic_sidebar('footer-right'); ?-->
        </div>
      
      </div><!-- /row -->
    </div><!-- /container -->
</footer><!-- end #footer -->




<?php wp_footer(); ?>

</body>
</html>