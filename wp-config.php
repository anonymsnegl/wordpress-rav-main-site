<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'RavWP');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~}*S+3C@sVh`9-|C 5@?M40 L[4%mRJ/S+q[R}a@ta5I;;}-c+@tP{V~++[Ud_er');
define('SECURE_AUTH_KEY',  'Xt->Mx%7z)Z4/7is<<i{ScT/)W^&%x)3gNe173:Lt%}Rkidf<{/?F)j{|FhUligu');
define('LOGGED_IN_KEY',    '@iTBRiQ&pS`?1<c}ix^VY`h_`Z;`8QR)x>>~bw$Xtch.&P+keHefs4+tdrq-3pC-');
define('NONCE_KEY',        '!|oQ~O,Q|kaP<)J+yA@Y&#fEY02!f-`WmqTb9uYa|#KzE+CUwWOci$l.Rxy?/jPv');
define('AUTH_SALT',        '<X;HA3-5D}XVg%O[FNy|1`_DU7I@5$1lFW^qb!yw%-&6_1_Xtd5^fQ-#-r{M<XKE');
define('SECURE_AUTH_SALT', '0~!7&,11y+O|[UM^t#eKBJq0#-?&@= ,v5J##jJ`,7T/2l5v|W7gk<}1~e}xyHYV');
define('LOGGED_IN_SALT',   '}<aD rWs_h=)cQ=<h7mn~~!}l>NMQ5{8eI@vs.+xOC5L] bw#Ka-%J-~ US%Er!2');
define('NONCE_SALT',       'C8o8<+%=dV0m2t>R{tnZ+)oZFyl+/-W[E6PJ.Np|oO4J-(kPJgnCnGUpt}n7tTGj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
